const {src, dest, watch, parallel} = require('gulp');
const notify = require('gulp-notify');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const plumber = require('gulp-plumber');
const rename = require('gulp-rename');
const postcss = require('gulp-postcss');
const purgecss = require('gulp-purgecss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const copy = require('gulp-copy');

//dir
var cssDir = './scripts/css/';
var jsDir = './scripts/js/';
var vendorDir = './scripts/vendor/';
var sassDir = './scripts/scss/*.scss';
var imgDir = './assets/';
var libDir = './lib/';
var scriptsDir = './scripts/';

//css plugin
var AirDateCss       = './lib/air-datepicker/css/datepicker.css';
var AnimateCss       = './lib/animate.css/css/animate.css';
var TagsInputCss     = './lib/bootstrap-tagsinput/css/bootstrap-tagsinput.css';
var TouchSpinCss     = './lib/bootstrap-touchspin/css/jquery.bootstrap-touchspin.css';
var CssgramCss       = './lib/cssgram/css/cssgram.css';
var FontAwsCss       = './lib/font-awesome/css/fontawesome.css';
var highlightCss     = './lib/highlightjs/css/default.css';
var iHover           = './lib/ihover/css/ihover.css';
var ImgHoverCss      = './lib/imagehover.css/css/imagehover.css';
var IonRangeCss      = './lib/ion.rangeSlider/css/*.css';
var IonIconsCss      = './lib/Ionicons/css/ionicons.css';
var LeafletCss       = './lib/leaflet/leaflet.css';
var MorrisCss        = './lib/morris.js/css/morris.css';
var OwlCaroselCss    = './lib/owl.carousel/css/owl.carousel.css';
var PerfectScrCss    = './lib/perfect-scrollbar/css/perfect-scrollbar.css';
var Select2Css       = './lib/select2/css/select2.css';
var SupfishCss       = './lib/superfish/css/superfish.css';
var SwiperCss        = './lib/swiper/css/swiper.css';

//js plugin
var AirDateJs        = './lib/air-datepicker/js/datepicker.js';
var BootstrapJs      = './lib/bootstrap/js/bootstrap.min.js';
var TagsInputJs      = './lib/bootstrap-tagsinput/js/bootstrap-tagsinput.js';
var TouchSpinJs      = './lib/bootstrap-touchspin/js/jquery.bootstrap-touchspin.js';
var ChartJs          = './lib/chart.js/js/Chart.js';
var RaphaelJs        = './lib/eve-raphael/js/eve.js';
var FontAwsJs        = './lib/font-awesome/css/fontawesome.css';
var GmapsJs          = './lib/gmaps/js/gmaps.js';
var HighlightJs      = './lib/highlightjs/js/highlight.pack.js';
var IonRangeJs       = './lib/ion.rangeSlider/js/*.js';
var JqueryJs         = './lib/jquery/js/jquery.min.js';
var JqueryHoverJs    = './lib/jquery-hoverintent/js/jquery.hoverIntent.js';
var JqueryAppearJs   = './lib/jquery_appear/jquery.appear.js';
var LeafletJs        = './lib/leaflet/leaflet-src.js';
var MochaJs          = './lib/mocha/js/mocha.js';
var ModernizrJs      = './lib/modernizr/js/Modernizr.js';
var MomentJs         = './lib/moment/js/moment.js';
var MorrisJs         = './lib/morris.js/js/morris.js';
var OwlCaroselJs     = './lib/owl.carousel/js/owl.carousel.min.js';
var PeityJs          = './lib/peity/js/jquery.peity.js';
var PerfectScrJs     = './lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js';
var PerfectScr2Js    = './lib/perfect-scrollbar/js/perfect-scrollbar.min.js';
var RaphaelJs        = './lib/raphael/js/raphael.js';
var Select2Js        = './lib/select2/js/select2.js';
var SupfishJs        = './lib/superfish/js/superfish.js';
var VanillaJs        = './lib/vanilla-lazyload/js/lazyload.js';
var PopperJs         = './lib/popper.js/js/popper.min.js';
var SwiperJs         = './lib/swiper/js/swiper.js';


// manage js asset
function jsAsset() {
   return src([
      JqueryJs, PopperJs, BootstrapJs, OwlCaroselJs, PerfectScrJs, PerfectScr2Js
   ])
      .pipe(plumber())
      .pipe(concat('plugin.min.js'))
      .pipe(dest(vendorDir))
      .pipe(notify({
         message: 'File <%= file.relative %> wis dadi'
      }));
}

// manage css asset
function cssAsset() {
   return src([bootstrap, animate])
      .pipe(plumber())
      .pipe(concat('plugin.css'))
      .pipe(postcss([autoprefixer(), cssnano()]))
      .pipe(dest(cssDir))
      .pipe(notify({
         message: 'File <%= file.relative %> wis dadi'
      }));
}

//sass-dev
function sassdev() {
   return src(sassDir)
      .pipe(plumber())
      .pipe(sass({
         errorLogToConsole: true
      }))
      .on('error', console.error.bind(console))
      .pipe(postcss([cssnano()]))
      .pipe(rename({
         suffix: '.min'
      }))
      .pipe(dest(cssDir))
      .pipe(browserSync.stream());
}

function sassdevNormal() {
   return src(sassDir)
      .pipe(plumber())
      .pipe(sass({
         errorLogToConsole: true
      }))
      .on('error', console.error.bind(console))
      .pipe(dest(cssDir))
      .pipe(browserSync.stream());
}

//minify css
function minify() {
   return src('./scripts/css/components.css')
      .pipe(postcss([cssnano()]))
      .pipe(rename({
         suffix: '.min'
      }))
      .pipe(dest(cssDir))
      .pipe(browserSync.stream());
}

//reload
function reload(done) {
   browserSync.reload();
   done();
}

//copy path
function copyPath() {
   return src(libDir)
      .pipe(copy())
      .pipe(dest(scriptsDir));
}


//watching scss and minify
function dev() {
   watch('./scripts/scss/**/*.scss', sassdev);
   watch('./scripts/scss/**/*.scss', sassdevNormal);
   watch('./scripts/css/components.css', minify);
}


exports.updateAsset  = parallel(cssAsset, jsAsset);
exports.updateCss    = cssAsset;
exports.updateJs     = jsAsset;
exports.minify       = minify;
exports.default      = parallel(sassdev, dev);
exports.copyPath     = copyPath;
exports.convertSass  = sassdev;

exports.sassdevNormal  = sassdevNormal;

exports.LiveDev      = dev, minify;