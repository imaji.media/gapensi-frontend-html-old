/* Custom Select Option */
/* author by : https://codepen.io/THEORLAN2/pen/Kaewmw */

window.onload = function () {
   crear_select();
}

function isMobileDevice() {
   return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};


var li = new Array();
function crear_select() {
   var div_cont_select = document.querySelectorAll("[data-mate-select='active']");
   var select_ = '';
   for (var e = 0; e < div_cont_select.length; e++) {
      div_cont_select[e].setAttribute('data-indx-select', e);
      div_cont_select[e].setAttribute('data-selec-open', 'false');
      var ul_cont = document.querySelectorAll("[data-indx-select='" + e + "'] > .cont_list_select_mate > ul");
      select_ = document.querySelectorAll("[data-indx-select='" + e + "'] >select")[0];
      if (isMobileDevice()) {
         select_.addEventListener('change', function () {
            _select_option(select_.selectedIndex, e);
         });
      }
      var select_optiones = select_.options;
      document.querySelectorAll("[data-indx-select='" + e + "']  > .selecionado_opcion ")[0].setAttribute('data-n-select', e);
      document.querySelectorAll("[data-indx-select='" + e + "']  > .icon_select_mate ")[0].setAttribute('data-n-select', e);
      for (var i = 0; i < select_optiones.length; i++) {
         li[i] = document.createElement('li');
         if (select_optiones[i].selected == true || select_.value == select_optiones[i].innerHTML) {
            li[i].className = 'active';
            document.querySelector("[data-indx-select='" + e + "']  > .selecionado_opcion ").innerHTML = select_optiones[i].innerHTML;
         };
         li[i].setAttribute('data-index', i);
         li[i].setAttribute('data-selec-index', e);
         // funcion click al selecionar 
         li[i].addEventListener('click', function () { _select_option(this.getAttribute('data-index'), this.getAttribute('data-selec-index')); });

         li[i].innerHTML = select_optiones[i].innerHTML;
         ul_cont[0].appendChild(li[i]);

      }; // Fin For select_optiones
   }; // fin for divs_cont_select
} // Fin Function 



var cont_slc = 0;
function open_select(idx) {
   var idx1 = idx.getAttribute('data-n-select');
   var ul_cont_li = document.querySelectorAll("[data-indx-select='" + idx1 + "'] .cont_select_int > li");
   var hg = 0;
   var slect_open = document.querySelectorAll("[data-indx-select='" + idx1 + "']")[0].getAttribute('data-selec-open');
   var slect_element_open = document.querySelectorAll("[data-indx-select='" + idx1 + "'] select")[0];
   if (isMobileDevice()) {
      if (window.document.createEvent) { // All
         var evt = window.document.createEvent("MouseEvents");
         evt.initMouseEvent("mousedown", false, true, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
         slect_element_open.dispatchEvent(evt);
      } else if (slect_element_open.fireEvent) { // IE
         slect_element_open.fireEvent("onmousedown");
      } else {
         slect_element_open.click();
      }
   } else {


      for (var i = 0; i < ul_cont_li.length; i++) {
         hg += ul_cont_li[i].offsetHeight;
      };
      if (slect_open == 'false') {
         document.querySelectorAll("[data-indx-select='" + idx1 + "']")[0].setAttribute('data-selec-open', 'true');
         document.querySelectorAll("[data-indx-select='" + idx1 + "'] > .cont_list_select_mate > ul")[0].style.height = hg + "px";
         document.querySelectorAll("[data-indx-select='" + idx1 + "'] > .cont_list_select_mate > ul")[0].style.maxHeight = 220 + "px";
         document.querySelectorAll("[data-indx-select='" + idx1 + "'] > .cont_list_select_mate > ul")[0].style.minHeight = "auto";
         document.querySelectorAll("[data-indx-select='" + idx1 + "'] > .icon_select_mate")[0].style.transform = 'rotate(180deg)';
      } else {
         document.querySelectorAll("[data-indx-select='" + idx1 + "']")[0].setAttribute('data-selec-open', 'false');
         document.querySelectorAll("[data-indx-select='" + idx1 + "'] > .icon_select_mate")[0].style.transform = 'rotate(0deg)';
         document.querySelectorAll("[data-indx-select='" + idx1 + "'] > .cont_list_select_mate > ul")[0].style.height = "0px";
         document.querySelectorAll("[data-indx-select='" + idx1 + "'] > .cont_list_select_mate > ul")[0].style.minHeight = "auto";
      }
   }

} // fin function open_select

function salir_select(indx) {
   var select_ = document.querySelectorAll("[data-indx-select='" + indx + "'] > select")[0];
   document.querySelectorAll("[data-indx-select='" + indx + "'] > .cont_list_select_mate > ul")[0].style.height = "0px";
   document.querySelector("[data-indx-select='" + indx + "'] > .icon_select_mate").style.transform = 'rotate(0deg)';
   document.querySelectorAll("[data-indx-select='" + indx + "']")[0].setAttribute('data-selec-open', 'false');
}


function _select_option(indx, selc) {
   if (isMobileDevice()) {
      selc = selc - 1;
   }
   var select_ = document.querySelectorAll("[data-indx-select='" + selc + "'] > select")[0];

   var li_s = document.querySelectorAll("[data-indx-select='" + selc + "'] .cont_select_int > li");
   var p_act = document.querySelectorAll("[data-indx-select='" + selc + "'] > .selecionado_opcion")[0].innerHTML = li_s[indx].innerHTML;
   var select_optiones = document.querySelectorAll("[data-indx-select='" + selc + "'] > select > option");
   for (var i = 0; i < li_s.length; i++) {
      if (li_s[i].className == 'active') {
         li_s[i].className = '';
      };
      li_s[indx].className = 'active';

   };
   select_optiones[indx].selected = true;
   select_.selectedIndex = indx;
   select_.onchange();
   salir_select(selc);
}


// start : perfect scrollbar js -->
$(document).ready(function () {
   'use strict';

   $('.c-leftpanel, .c-rightpanel').perfectScrollbar();

   if ($(document).scrollTop() > 10) {
      $('.c-headpanel').addClass('u-top--neg-70');
   }

   $(window).scroll(function () {
      if ($(document).scrollTop() > 10) {
         $('.c-headpanel').addClass('u-top--neg-70');
         $('.is-sticky').addClass('u-mrgn-top--2p');
      } else {
         $('.c-headpanel').removeClass('u-top--neg-70');
         $('.is-sticky').removeClass('u-mrgn-top--2p');
      }
   });

   if ($(document).scrollTop() > 10) {
      $('.c-headpanel-minisite').addClass('u-top--neg-40');
   }

   $(window).scroll(function () {
      if ($(document).scrollTop() > 10) {
         $('.c-headpanel-minisite').addClass('u-top--neg-40');
      } else {
         $('.c-headpanel-minisite').removeClass('u-top--neg-40');
      }
   });
});
// end : perfect scrollbar js -->


// start : initialize slider -->
var swiper = new Swiper('.swiper-home-slider', {
   loop: true,
   pagination: {
      el: '.swiper-pagination',
      clickable: true,
   },
   navigation: {
      nextEl: '.next-home-slider',
      prevEl: '.prev-home-slider',
   },
   autoplay: {
      delay: 8500,
      disableOnInteraction: false,
   },
});
// end : initialize slider -->


// start : initialize news info -->
var swiper = new Swiper('.swiper-news-info', {
   spaceBetween: 25,
   navigation: {
      nextEl: '.next-news-info',
      prevEl: '.prev-news-info',
   },
});
// end : initialize news info -->


// start : initialize pengurus pusat -->
var swiper = new Swiper('.swiper-list-admin', {
   slidesPerView: 4,
   spaceBetween: 14.8,
   navigation: {
      nextEl: '.next-list-admin',
      prevEl: '.prev-list-admin',
   },
});
// end : initialize pengurus pusat -->


// start : initialize berita gapensi -->
var swiper = new Swiper('.swiper-list-news', {
   slidesPerView: 5,
   spaceBetween: 0,
   navigation: {
      nextEl: '.next-list-news',
      prevEl: '.prev-list-news',
   },
});
// end : initialize berita gapensi -->

// start : initialize slider -->
var swiper = new Swiper('.swiper-minisite-banner', {
   loop: true,
   pagination: {
      el: '.swiper-pagination',
      clickable: true,
   },
   navigation: {
      nextEl: '.next-minisite-banner',
      prevEl: '.prev-minisite-banner',
   },
   autoplay: {
      delay: 8500,
      disableOnInteraction: false,
   },
});
// end : initialize slider -->


// start : calendar event with Json and Jquery -->
function get_count_diff_date(data1, data2) {
   time = 1000 * 60 * 60 * 24;

   data1z = data1;
   data2z = data2;

   var nova1 = data1z.toString().split('/');
   Nova1 = nova1[1] + "/" + nova1[0] + "/" + nova1[2];

   var nova2 = data2z.toString().split('/');
   Nova2 = nova2[1] + "/" + nova2[0] + "/" + nova2[2];

   d1 = new Date(Nova1);
   d2 = new Date(Nova2);

   count = Math.round((d2.getTime() - d1.getTime()) / time);
   return count;
}

var events = [
   { ID: 1, Title: "Acara 1", Date: new Date("11/03/2017"), Date_2: new Date("11/05/2017"), Tipo: "Wisuda" },
   { ID: 3, Title: "Acara 3", Date: new Date("11/08/2017"), Date_2: new Date("11/10/2017"), Tipo: "Wisuda" },
   { ID: 6, Title: "Acara 5", Date: new Date("11/15/2017"), Date_2: new Date("11/28/2017"), Tipo: "Wisuda" },
];

// all monthly
$(function () {
   $("#calendario").datepicker({
      dateFormat: "dd/mm/yy",
      dayNames: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"],
      dayNamesMin: ["M", "S", "S", "R", "K", "J", "S", "M"],
      dayNamesShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Min"],
      monthNames: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
      monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
      nextText: "Selanjutnya",
      prevText: "Sebelumnya",
      beforeShowDay: function (date) {
         var result = [true, "", null];
         var matching = $.grep(events, function (event) {
            return event.Date.valueOf() === date.valueOf();
         });
         if (matching.length) {
            result = [true, "highlight", matching[0].Title];
         }

         var result2 = [true, "", null];
         var matching2 = $.grep(events, function (event) {
            if (event.Date_2 == null) {
               return '';
            } else {
               return event.Date_2.valueOf() === date.valueOf();
            }
         });
         if (matching2.length) {
            result = [true, "highlight", matching2[0].Title];
         }

         return result;
      }
   });
});

// start : calendar monthly custom
$(function () {
   $("#MonthJan").datepicker({
      defaultDate: new Date (2019, 0 , 1),
      dateFormat: "dd/mm/yy",
      dayNames: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"],
      dayNamesMin: ["M", "S", "S", "R", "K", "J", "S", "M"],
      dayNamesShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Min"],
      monthNames: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
      monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
      changeMonth: false,
      changeYear: false,
      showOtherMonths: false,
      stepMonths: 0,
      beforeShowDay: function (date) {
         var result = [true, "", null];
         var matching = $.grep(events, function (event) {
            return event.Date.valueOf() === date.valueOf();
         });
         if (matching.length) {
            result = [true, "highlight", matching[0].Title];
         }

         var result2 = [true, "", null];
         var matching2 = $.grep(events, function (event) {
            if (event.Date_2 == null) {
               return '';
            } else {
               return event.Date_2.valueOf() === date.valueOf();
            }
         });
         if (matching2.length) {
            result = [true, "highlight", matching2[0].Title];
         }

         return result;
      }
   });
});

$(function () {
   $("#MonthFeb").datepicker({
      defaultDate: new Date (2019, 1 ,1),
      dateFormat: "dd/mm/yy",
      dayNames: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"],
      dayNamesMin: ["M", "S", "S", "R", "K", "J", "S", "M"],
      dayNamesShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Min"],
      monthNames: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
      monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
      changeMonth: false,
      changeYear: false,
      showOtherMonths: false,
      stepMonths: 0,
      beforeShowDay: function (date) {
         var result = [true, "", null];
         var matching = $.grep(events, function (event) {
            return event.Date.valueOf() === date.valueOf();
         });
         if (matching.length) {
            result = [true, "highlight", matching[0].Title];
         }

         var result2 = [true, "", null];
         var matching2 = $.grep(events, function (event) {
            if (event.Date_2 == null) {
               return '';
            } else {
               return event.Date_2.valueOf() === date.valueOf();
            }
         });
         if (matching2.length) {
            result = [true, "highlight", matching2[0].Title];
         }

         return result;
      }
   });
});

$(function () {
   $("#MonthMar").datepicker({
      defaultDate: new Date (2019, 2, 1),
      dateFormat: "dd/mm/yy",
      dayNames: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"],
      dayNamesMin: ["M", "S", "S", "R", "K", "J", "S", "M"],
      dayNamesShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Min"],
      monthNames: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
      monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
      changeMonth: false,
      changeYear: false,
      showOtherMonths: false,
      stepMonths: 0,
      beforeShowDay: function (date) {
         var result = [true, "", null];
         var matching = $.grep(events, function (event) {
            return event.Date.valueOf() === date.valueOf();
         });
         if (matching.length) {
            result = [true, "highlight", matching[0].Title];
         }

         var result2 = [true, "", null];
         var matching2 = $.grep(events, function (event) {
            if (event.Date_2 == null) {
               return '';
            } else {
               return event.Date_2.valueOf() === date.valueOf();
            }
         });
         if (matching2.length) {
            result = [true, "highlight", matching2[0].Title];
         }

         return result;
      }
   });
});

$(function () {
   $("#MonthApr").datepicker({
      defaultDate: new Date (2019, 3, 1),
      dateFormat: "dd/mm/yy",
      dayNames: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"],
      dayNamesMin: ["M", "S", "S", "R", "K", "J", "S", "M"],
      dayNamesShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Min"],
      monthNames: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
      monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
      changeMonth: false,
      changeYear: false,
      showOtherMonths: false,
      stepMonths: 0,
      beforeShowDay: function (date) {
         var result = [true, "", null];
         var matching = $.grep(events, function (event) {
            return event.Date.valueOf() === date.valueOf();
         });
         if (matching.length) {
            result = [true, "highlight", matching[0].Title];
         }

         var result2 = [true, "", null];
         var matching2 = $.grep(events, function (event) {
            if (event.Date_2 == null) {
               return '';
            } else {
               return event.Date_2.valueOf() === date.valueOf();
            }
         });
         if (matching2.length) {
            result = [true, "highlight", matching2[0].Title];
         }

         return result;
      }
   });
});

$(function () {
   $("#MonthMei").datepicker({
      defaultDate: new Date (2019, 4, 1),
      dateFormat: "dd/mm/yy",
      dayNames: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"],
      dayNamesMin: ["M", "S", "S", "R", "K", "J", "S", "M"],
      dayNamesShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Min"],
      monthNames: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
      monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
      changeMonth: false,
      changeYear: false,
      showOtherMonths: false,
      stepMonths: 0,
      beforeShowDay: function (date) {
         var result = [true, "", null];
         var matching = $.grep(events, function (event) {
            return event.Date.valueOf() === date.valueOf();
         });
         if (matching.length) {
            result = [true, "highlight", matching[0].Title];
         }

         var result2 = [true, "", null];
         var matching2 = $.grep(events, function (event) {
            if (event.Date_2 == null) {
               return '';
            } else {
               return event.Date_2.valueOf() === date.valueOf();
            }
         });
         if (matching2.length) {
            result = [true, "highlight", matching2[0].Title];
         }

         return result;
      }
   });
});

$(function () {
   $("#MonthJun").datepicker({
      defaultDate: new Date (2019, 5, 1),
      dateFormat: "dd/mm/yy",
      dayNames: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"],
      dayNamesMin: ["M", "S", "S", "R", "K", "J", "S", "M"],
      dayNamesShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Min"],
      monthNames: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
      monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
      changeMonth: false,
      changeYear: false,
      showOtherMonths: false,
      stepMonths: 0,
      beforeShowDay: function (date) {
         var result = [true, "", null];
         var matching = $.grep(events, function (event) {
            return event.Date.valueOf() === date.valueOf();
         });
         if (matching.length) {
            result = [true, "highlight", matching[0].Title];
         }

         var result2 = [true, "", null];
         var matching2 = $.grep(events, function (event) {
            if (event.Date_2 == null) {
               return '';
            } else {
               return event.Date_2.valueOf() === date.valueOf();
            }
         });
         if (matching2.length) {
            result = [true, "highlight", matching2[0].Title];
         }

         return result;
      }
   });
});

$(function () {
   $("#MonthJul").datepicker({
      defaultDate: new Date (2019, 6, 1),
      dateFormat: "dd/mm/yy",
      dayNames: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"],
      dayNamesMin: ["M", "S", "S", "R", "K", "J", "S", "M"],
      dayNamesShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Min"],
      monthNames: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
      monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
      changeMonth: false,
      changeYear: false,
      showOtherMonths: false,
      stepMonths: 0,
      beforeShowDay: function (date) {
         var result = [true, "", null];
         var matching = $.grep(events, function (event) {
            return event.Date.valueOf() === date.valueOf();
         });
         if (matching.length) {
            result = [true, "highlight", matching[0].Title];
         }

         var result2 = [true, "", null];
         var matching2 = $.grep(events, function (event) {
            if (event.Date_2 == null) {
               return '';
            } else {
               return event.Date_2.valueOf() === date.valueOf();
            }
         });
         if (matching2.length) {
            result = [true, "highlight", matching2[0].Title];
         }

         return result;
      }
   });
});

$(function () {
   $("#MonthAgu").datepicker({
      defaultDate: new Date (2019, 7, 1),
      dateFormat: "dd/mm/yy",
      dayNames: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"],
      dayNamesMin: ["M", "S", "S", "R", "K", "J", "S", "M"],
      dayNamesShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Min"],
      monthNames: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
      monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
      changeMonth: false,
      changeYear: false,
      showOtherMonths: false,
      stepMonths: 0,
      beforeShowDay: function (date) {
         var result = [true, "", null];
         var matching = $.grep(events, function (event) {
            return event.Date.valueOf() === date.valueOf();
         });
         if (matching.length) {
            result = [true, "highlight", matching[0].Title];
         }

         var result2 = [true, "", null];
         var matching2 = $.grep(events, function (event) {
            if (event.Date_2 == null) {
               return '';
            } else {
               return event.Date_2.valueOf() === date.valueOf();
            }
         });
         if (matching2.length) {
            result = [true, "highlight", matching2[0].Title];
         }

         return result;
      }
   });
});

$(function () {
   $("#MonthSep").datepicker({
      defaultDate: new Date (2019, 8, 1),
      dateFormat: "dd/mm/yy",
      dayNames: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"],
      dayNamesMin: ["M", "S", "S", "R", "K", "J", "S", "M"],
      dayNamesShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Min"],
      monthNames: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
      monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
      changeMonth: false,
      changeYear: false,
      showOtherMonths: false,
      stepMonths: 0,
      beforeShowDay: function (date) {
         var result = [true, "", null];
         var matching = $.grep(events, function (event) {
            return event.Date.valueOf() === date.valueOf();
         });
         if (matching.length) {
            result = [true, "highlight", matching[0].Title];
         }

         var result2 = [true, "", null];
         var matching2 = $.grep(events, function (event) {
            if (event.Date_2 == null) {
               return '';
            } else {
               return event.Date_2.valueOf() === date.valueOf();
            }
         });
         if (matching2.length) {
            result = [true, "highlight", matching2[0].Title];
         }

         return result;
      }
   });
});

$(function () {
   $("#MonthOkt").datepicker({
      defaultDate: new Date (2019, 9, 1),
      dateFormat: "dd/mm/yy",
      dayNames: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"],
      dayNamesMin: ["M", "S", "S", "R", "K", "J", "S", "M"],
      dayNamesShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Min"],
      monthNames: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
      monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
      changeMonth: false,
      changeYear: false,
      showOtherMonths: false,
      stepMonths: 0,
      beforeShowDay: function (date) {
         var result = [true, "", null];
         var matching = $.grep(events, function (event) {
            return event.Date.valueOf() === date.valueOf();
         });
         if (matching.length) {
            result = [true, "highlight", matching[0].Title];
         }

         var result2 = [true, "", null];
         var matching2 = $.grep(events, function (event) {
            if (event.Date_2 == null) {
               return '';
            } else {
               return event.Date_2.valueOf() === date.valueOf();
            }
         });
         if (matching2.length) {
            result = [true, "highlight", matching2[0].Title];
         }

         return result;
      }
   });
});

$(function () {
   $("#MonthNov").datepicker({
      defaultDate: new Date (2019, 10, 1),
      dateFormat: "dd/mm/yy",
      dayNames: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"],
      dayNamesMin: ["M", "S", "S", "R", "K", "J", "S", "M"],
      dayNamesShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Min"],
      monthNames: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
      monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
      changeMonth: false,
      changeYear: false,
      showOtherMonths: false,
      stepMonths: 0,
      beforeShowDay: function (date) {
         var result = [true, "", null];
         var matching = $.grep(events, function (event) {
            return event.Date.valueOf() === date.valueOf();
         });
         if (matching.length) {
            result = [true, "highlight", matching[0].Title];
         }

         var result2 = [true, "", null];
         var matching2 = $.grep(events, function (event) {
            if (event.Date_2 == null) {
               return '';
            } else {
               return event.Date_2.valueOf() === date.valueOf();
            }
         });
         if (matching2.length) {
            result = [true, "highlight", matching2[0].Title];
         }

         return result;
      }
   });
});

$(function () {
   $("#MonthDes").datepicker({
      defaultDate: new Date (2019, 11, 1),
      dateFormat: "dd/mm/yy",
      dayNames: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"],
      dayNamesMin: ["M", "S", "S", "R", "K", "J", "S", "M"],
      dayNamesShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Min"],
      monthNames: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
      monthNamesShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"],
      changeMonth: false,
      changeYear: false,
      showOtherMonths: false,
      stepMonths: 0,
      beforeShowDay: function (date) {
         var result = [true, "", null];
         var matching = $.grep(events, function (event) {
            return event.Date.valueOf() === date.valueOf();
         });
         if (matching.length) {
            result = [true, "highlight", matching[0].Title];
         }

         var result2 = [true, "", null];
         var matching2 = $.grep(events, function (event) {
            if (event.Date_2 == null) {
               return '';
            } else {
               return event.Date_2.valueOf() === date.valueOf();
            }
         });
         if (matching2.length) {
            result = [true, "highlight", matching2[0].Title];
         }

         return result;
      }
   });
});
// end : calendar monthly custom

$(function () {
   //$.grep(events, function(event) {
   $.each(events, function (index, value) {

      var id = value.ID;
      var title = value.Title;
      var date1 = value.Date;
      var date2 = value.Date_2;
      var tipo = value.Tipo;

      /* 1 dia */
      var data = date1;
      var dia = data.getDate();
      if (dia.toString().length == 1) {
         dia = "0" + dia;
      }

      var mes = data.getMonth() + 1;
      if (mes.toString().length == 1) {
         mes = "0" + mes;
      }

      var ano = data.getFullYear();
      data_final = dia + "/" + mes + "/" + ano;
      /* 1 dia */

      /* 2 dia */
      var data2 = date2;
      if (data2 == null) {
         dia2 = "";
         mes2 = "";
         ano2 = "";
         data_final2 = "";
      } else {
         var dia2 = data2.getDate();
         if (dia2.toString().length == 1) {
            dia2 = "0" + dia2;
         }

         var mes2 = data2.getMonth() + 1;
         if (mes2.toString().length == 1) {
            mes2 = "0" + mes2;
         }

         var ano2 = data2.getFullYear();
         data_final2 = dia2 + "/" + mes2 + "/" + ano2;
      }
      /* 2 dia */

      $("#eventos").append(
         "<div class='eventos' data-date='" + data_final + "' data-date2='" + data_final2 + "'>" +
         title +
         " - " +
         data_final +
         " - " +
         tipo +
         " - " +
         data_final2 +
         "</div>"
      );

   });

   $("#eventos").on("mouseover", "div", function () {
      var date = $(this).data("date");
      var date2 = $(this).data("date2");

      var dia = date.toString().split('/');
      dia_f = dia[0]; // this day

      $('#calendario').datepicker('setDate', date);

   });

});
// end : calendar event with Json and Jquery -->


// start : magnific popup - single image
$(document).ready(function() {

	$('.image-popup-vertical-fit').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile',
		image: {
			verticalFit: true
		}
		
	});

	$('.image-popup-fit-width').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		image: {
			verticalFit: false
		}
	});

	$('.image-popup-no-margins').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		closeBtnInside: false,
		fixedContentPos: true,
		mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
		image: {
			verticalFit: true
		},
		zoom: {
			enabled: true,
			duration: 300 // don't foget to change the duration also in CSS
		}
	});
});
// end : magnific popup - single image


// start : magnific popup - youtube
$(document).ready(function() {
	$('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: false
	});
});
// end : magnific popup - youtube


// start : initialize fasilitas kantor -->
var swiper = new Swiper('.swiper-fasilitas', {
   slidesPerView: 8,
   spaceBetween: 12.8,
   navigation: {
      nextEl: '.next-fasilitas',
      prevEl: '.prev-fasilitas',
   },
});
// end : initialize fasilitas kantor -->


// start : smooth scroll effect with id -->
// header effect on scroll
function animateHead() {
   if ($(document).scrollTop() > 100) {
      $('#headPanel').addClass('head-scroll');
   } else {
      $('#headPanel').removeClass('head-scroll');
   }

   if ($(document).scrollTop() > 500) {
      $('#headPanel').addClass('head-scrolled');
   } else {
      $('#headPanel').removeClass('head-scrolled');
   }
}

animateHead();

$(window).scroll(function () {
   animateHead();
});

// animated smooth scroll on target from top menu
$('#navbarMain .c-nav__link').on('click', function (e) {

   var target = $(this).attr('href');
   $('html, body').animate({
      scrollTop: $('' + target).offset().top
   }, 500);

   e.preventDefault();
});


// navbar main is active
var header = document.getElementById("navbarMain");
var a = header.getElementsByClassName("c-menu-link");
for (var i = 0; i < a.length; i++) {
  a[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("is-active");
  if (current.length > 0) { 
    current[0].className = current[0].className.replace(" is-active", "");
  }
  this.className += " is-active";
  });
}


// accordion custom
$('.panel-collapse').on('show.bs.collapse', function () {
   $(this).siblings('.panel-heading').addClass('active');
});

$('.panel-collapse').on('hide.bs.collapse', function () {
   $(this).siblings('.panel-heading').removeClass('active');
});


// show more provinsi
$('.button-expand__link.provinsi').click(function() {
   $('.show-more.provinsi').slideToggle();
   if ($('.button-expand__link.provinsi').text() == "29 Provinsi Lainnya") {
     $(this).text("Lihat Sebagian")
   } else {
     $(this).text("29 Provinsi Lainnya")
   }
});

// show more kota/kabupaten
$('.button-expand__link.kota-kab').click(function() {
   $('.show-more.kota-kab').slideToggle();
   if ($('.button-expand__link.kota-kab').text() == "Kota/Kabupaten Lainnya") {
     $(this).text("Lihat Sebagian")
   } else {
     $(this).text("Kota/Kabupaten Lainnya")
   }
});

// show more kualifikasi
$('.button-expand__link.kualifikasi').click(function() {
   $('.show-more.kualifikasi').slideToggle();
   if ($('.button-expand__link.kualifikasi').text() == "Lihat Lainnya") {
     $(this).text("Lihat Sebagian")
   } else {
     $(this).text("Lihat Lainnya")
   }
});

// show more badan-usaha
$('.button-expand__link.badan-usaha').click(function() {
   $('.show-more.badan-usaha').slideToggle();
   if ($('.button-expand__link.badan-usaha').text() == "Lihat Lainnya") {
     $(this).text("Lihat Sebagian")
   } else {
     $(this).text("Lihat Lainnya")
   }
});