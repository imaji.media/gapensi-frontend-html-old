
'use strict';

module.exports = function (grunt) {

   // Load grunt tasks automatically
   require('load-grunt-tasks')(grunt);

   // Time how long tasks take. Can help when optimizing build times
   require('time-grunt')(grunt);

   // Configurable paths for the application
   var app = require('./package.json');
   var appConfig = {
      name: app.name
   };

   // Define the configuration for all the tasks
   grunt.initConfig({

      // Project settings
      pkg: appConfig,

      watch: {
         bower: {
            files: ['bower.json'],
            tasks: ['build']
         },
         scss: {
            files: ['scripts/scss/**/*.scss'],
            tasks: ['sass:dev']
         },
         html: {
            files: ['pages/{,*/}*.html', 'demo/{,*/}*.html', 'templates/{,*/}*.html'],
            options: {
               livereload: '<%= connect.options.livereload %>'
            }
         },
         css: {
            files: ['scripts/css/{,*/}*.css'],
            options: {
               livereload: '<%= connect.options.livereload %>'
            }
         },
         js: {
            files: ['scripts/js/{,*/}*.js'],
            options: {
               livereload: '<%= connect.options.livereload %>'
            }
         }
      },

      // The actual grunt server settings
      connect: {
         options: {
            port: 9000,
            open: true,
            livereload: 35729,
            // Change this to '0.0.0.0' to access the server from outside
            hostname: 'localhost'
         },
         templates: {
            options: {
               open: true,
               middleware: function (connect) {
                  return [
                     connect().use('/bower_components', connect.static('./bower_components')),
                     connect().use('/lib', connect.static('./lib')),
                     connect().use('/assets/images', connect.static('./assets/images')),
                     connect().use('/assets/videos', connect.static('./assets/videos')),
                     connect().use('/scripts/css', connect.static('./scripts/css')),
                     connect().use('/scripts/js', connect.static('./scripts/js')),
                     connect().use('/root/bpp', connect.static('./root/bpp')),
                     connect().use('/root/bpd', connect.static('./root/bpd')),
                     connect().use('/root/bpc', connect.static('./root/bpc')),
                     connect().use('/root/personal', connect.static('./root/personal')),
                     connect.static('templates'),
                  ];
               }
            }
         }
      },

      // Compiles scss files to css
      sass: {
         dist: {
            options: { style: 'compressed', sourcemap: 'none' },
            files: { 
               'scripts/css/utilities.min.css'  : 'scripts/scss/utilities.scss',
               'scripts/css/templates.min.css'  : 'scripts/scss/templates.scss',
               'scripts/css/layout.min.css'     : 'scripts/scss/layout.scss',
               'scripts/css/components.min.css' : 'scripts/scss/components.css'
            }
         },
         dev: {
            options: { style: 'expanded', sourcemap: 'none' },
            files: {
               'scripts/css/utilities.css'      : 'scripts/scss/utilities.scss',
               'scripts/css/templates.css'      : 'scripts/scss/templates.scss',
               'scripts/css/layout.css'         : 'scripts/scss/layout.scss',
            }
         }
      },

      // Make sure code styles are up to par and there are no obvious mistakes
      jshint: {
         options: {
            jshintrc: '.jshintrc',
            reporter: require('jshint-stylish')
         },
         all: {
            src: [
               'Gruntfile.js',
               'scripts/js/**/*.js'
            ]
         }
      },

      bower: {
         install: {
            options: {
               cleanTargetDir: true,
               layout: 'byComponent'
            }
         }
      }
   });

   grunt.registerTask('default', [
      'sass',
      'connect',
      'watch'
   ]);

   grunt.registerTask('build', ['bower']);

};
